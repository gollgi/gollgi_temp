<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('APP_PATH', realpath('.'));

require_once (APP_PATH . "/TempTable.php");

if (isAjax()) {
    $action = $_GET['action'];
    switch($action) {
        case 'change_state':
            echo json_encode(TempTable::changeState());
            break;
        case 'get_state':
            echo json_encode(TempTable::getById($_GET['request_id']));
            break;
    }
} else {
    $all = TempTable::getAll();
    include (APP_PATH . "/index_view.php");
}

function isAjax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
}

