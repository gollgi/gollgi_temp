<?php

require_once (APP_PATH . "/Database.php");

class TempTable
{
    public $id;
    public $create_time;
    public $state;

    public function getById($state_id) {
        $query = "SELECT * FROM temp WHERE id='{$state_id}'";
        $result = pg_query(Database::getInstance(), $query);
        if ($result) {
            return pg_fetch_assoc($result);
        }
        return null;
    }

    public function getLast() {
        $query = "SELECT * FROM temp ORDER BY create_time DESC LIMIT 1";

        $result = pg_query(Database::getInstance(), $query);
        if ($result) {
            return pg_fetch_assoc($result);
        }
        return null;
    }

    public static function getAll() {
        $query = "SELECT * FROM temp ORDER BY create_time DESC";

        $result = pg_query(Database::getInstance(), $query);
        if ($result) {
            $rows = [];
            while ($row = pg_fetch_assoc($result)) {
                $rows[] = $row;
            }
            return $rows;
        }
        return null;
    }

    public function insert(int $state) {
        $query = "INSERT INTO temp(state) VALUES({$state}) RETURNING id, create_time";
        $result = pg_query(Database::getInstance(), $query);
        if ($result) {
            $data =  pg_fetch_assoc($result);
            if ($data) {
                $ret = new TempTable();
                $ret->state = $state;
                $ret->id = $data['id'];
                $ret->create_time = $data['create_time'];
                return $ret;
            }
        }
        return null;
    }

    public static function changeState() {
        $query = "INSERT INTO temp(state) SELECT (1 - state) FROM temp ORDER BY create_time DESC LIMIT 1 RETURNING *";
        $result = pg_query(Database::getInstance(), $query);
        if ($result) {
            return pg_fetch_assoc($result);
        }
        return null;
    }
}