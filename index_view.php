<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title>Gollgi Temp</title>
</head>
<body>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<button onclick="change_state()">Change State</button>
<table id="states_table">
    <thead>
    <tr>
        <td>id</td>
        <td>time</td>
        <td>state</td>
    </tr>
    </thead>
    <tbody>
    <?php foreach($all as $row) { ?>
        <tr>
            <td> <?= $row['id'] ?></td>
            <td> <?= $row['create_time'] ?></td>
            <td> <?= $row['state'] ? "On" : "Off" ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</body>
<script>
    let socket = null;
    $(function() {
        socket = io.connect('temp.gollgi.info:5433');

        socket.on('state.changed', function(state_id) {
            $.get("index.php", {action: 'change_state', state_id: state_id},function(data) {
                data = JSON.parse(data);
                let tr = $("<tr/>").append($("<td/>").text(data.id)).append($("<td/>").text(data.create_time)).append($("<td/>").text(data.state ? "On" : "Off"))
                $("tbody").append(tr)
            })
        })
    })

    function change_state() {
        $.get("index.php", {action: 'change_state'},function(data) {
            data = JSON.parse(data);
            socket.emit('change.state', data.id)
        })
    }
</script>
<script type="text/javascript" src="/js/socket.io/socket.io.js"></script>
</html>