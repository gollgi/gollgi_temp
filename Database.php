<?php

require_once (APP_PATH . "/config.php");

class Database
{
    protected static $instance = null;

    protected function __construct()
    {
    }

    public static function getInstance() {
        if (self::$instance === null) {
            foreach(DB_CONFIG as $key=>$val) {
                $conn_parts[] = "{$key}={$val}";
            }
            self::$instance = pg_connect ( implode(" ",$conn_parts) ) OR die("fail");
        }
        return self::$instance;
    }
}