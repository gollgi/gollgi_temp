#!/usr/bin/env node

let app = require('express')();
let fs = require('fs');
let options = { // dev
    allowUpgrades: true,
    key: fs.readFileSync('/etc/apache2/gollgi.info/gollgi_info_ssl.key'),
    cert: fs.readFileSync('/etc/apache2/gollgi.info/b546854cc6b27f60.crt'),
    ca: fs.readFileSync('/etc/apache2/gollgi.info/gd_bundle-g2-g1.crt')
};

let transport = require('https').createServer(options, app);
let io = require('socket.io')(transport);
//let middleware = require('socketio-wildcard')();
//io.use(middleware);

io.on('connection', function (socket) {
    console.log('connected ', socket.conn.remoteAddress, socket.id);
    socket.on('change.state', function (data) {
        console.log(data);
        socket.emit('state.changed', data)
    });
});

transport.listen(5433, function () {
    console.log('listening on *:5433');
});


//////////////
//php listener
//////////////

// let redis = require("redis");
// let php = redis.createClient();
//
// php.on("connect", function (socket) {
//     console.log('php connect');
// });
//
// php.on('error', function (err) {
//     console.log('Something went wrong ' + err);
// });
//
// php.on("message", function(pattern, key){
//     console.log('php message', pattern, key);
// })
//
// php.on("event", function(pattern, key){
//     console.log('php event', pattern, key);
// })


// let p_transport = require('https').createServer(options, app);
// let p_io = require('socket.io')(p_transport);
// let redis = require('socket.io-redis');
//
// p_io.adapter(redis({ host: '127.0.0.1', port: 6379 }));
//
// p_io.on('connection', function(socket){
//     console.log('client connected'); // Works
//
//     socket.emit('connect','test'); // Works
// });
//
// p_transport.listen(3000);
